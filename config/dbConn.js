const mongoose = require('mongoose');
const connectionDB = async () => {
  try {
    await mongoose.connect(process.env.CONNECTION_STRING, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
    });
  } catch (error) {
    console.log('Database connection error', error.message);
  }
};
module.exports = connectionDB;
