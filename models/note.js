const {Schema, model} = require('mongoose');

const NoteSchema = new Schema({
  userId: String,
  completed: Boolean,
  text: String,
  createdDate: Date,
});

module.exports = model('Note', NoteSchema);
