const User = require('../models/user');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const handleLogin = async (req, res) => {
  const {username, password} = req.body;
  if (!username || !password) {
    return res
        .status(400)
        .json({message: 'Username and password are required'});
  }

  try {
    const foundUser = await User.findOne({username: username}).exec();
    if (!foundUser) {
      return res.status(400).json({
        message: `User ${username} Unauthorized and not found in system`,
      });
    }
    // evaluate password
    const match = await bcrypt.compare(password, foundUser.password);
    if (match) {
      const accsessToken = jwt.sign(
          {
            id: foundUser._id,
          },
          process.env.ACCESS_TOKEN_SECRET,
      );

      res.status(200).json({message: 'Success', jwt_token: accsessToken});
    } else {
      return res.status(401).json({message: `User ${username} Unauthorized`});
    }
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports = {handleLogin};
