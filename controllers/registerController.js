const User = require('../models/user');
const bcrypt = require('bcrypt');

const handleNewUser = async (req, res) => {
  const {username, password} = req.body;
  if (!username || !password) {
    return res
        .status(400)
        .json({message: 'Username and Password a required'});
  }
  // check for duplicate usernames in db
  const duplicate = await User.findOne({username: username}).exec();
  if (duplicate) {
    return res
        .status(400)
        .json({message: `User: ${username} already exists`});
  }
  try {
    // encrypt the password
    const hashedPwd = await bcrypt.hash(password, 10);
    // create and store the new user
    await User.create({
      username: username,
      password: hashedPwd,
      createdDate: new Date(),
    });

    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports = {handleNewUser};
