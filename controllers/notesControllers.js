const Note = require('../models/note');
const getUserId = require('../helpers/getUserId');

const postNote = async (req, res) => {
  try {
    const ID = getUserId(req);
    if (!req.body.text) {
      res.status(400).json({message: 'Empty field'});
    }
    await Note.create({
      userId: ID,
      completed: false,
      text: req.body.text,
      createdDate: new Date(),
    });

    res.status(200).json({message: 'Success'});
  } catch (error) {
    res.status(500).json({message: error.message});
  }
};

const getNotes = async (req, res) => {
  const ID = getUserId(req);
  try {
    const notes = await Note.find({
      userId: ID,
    }).exec();

    if (!notes) {
      res.status(400).json({message: 'Notes not found'});
    }

    res.status(200).json({
      offset: notes.length - 5,
      limit: 5,
      count: notes.length,
      notes: notes.length > 5 ? notes.slice(-5) : notes,
    });
  } catch (error) {
    res.status(500).json({message: error.message});
  }
};

const getNoteById = async (req, res) => {
  try {
    const note = await Note.findById(req.params.id).exec();

    if (!note) {
      res.status(400).json({message: 'Note not found'});
    }
    res.status(200).json({note});
  } catch (error) {
    res.status(500).json({message: error.message});
  }
};

const updateUsersNote = async (req, res) => {
  try {
    if (!req.body.text) {
      res.status(400).json({message: 'Empty field'});
    }
    await Note.findByIdAndUpdate(req.params.id, {text: req.body.text}).exec();

    res.status(200).json({message: 'Success'});
  } catch (error) {
    res.status(500).json({message: error.message});
  }
};

const updateCheckedInNote = async (req, res) => {
  try {
    if (!req.params.id) {
      res.status(400).json({message: 'Bad request no notes id in params'});
    }
    const note = await Note.findById(req.params.id).exec();
    if (!note) {
      res.status(400).json({message: 'Note not found'});
    }

    await Note.findByIdAndUpdate(req.params.id, {
      completed: !note.completed,
    }).exec();

    res.status(200).json({message: 'Success'});
  } catch (error) {
    res.status(500).json({message: error.message});
  }
};

const deleteNote = async (req, res) => {
  try {
    const note = await Note.findById(req.params.id).exec();
    if (!note) {
      res.status(400).json({message: 'Note not found'});
    }
    await Note.findByIdAndDelete(req.params.id).exec();
    res.status(200).send({message: 'Success'});
  } catch (error) {
    res.status(500).send({message: error.message});
  }
};

module.exports = {
  postNote,
  getNotes,
  getNoteById,
  updateUsersNote,
  updateCheckedInNote,
  deleteNote,
};
