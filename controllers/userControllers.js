const User = require('../models/user');
const getUserId = require('../helpers/getUserId');
const bcrypt = require('bcrypt');

const getUserById = async (req, res) => {
  try {
    const ID = getUserId(req);

    const foundUser = await User.findById(ID).exec();

    if (!foundUser) {
      return res.status(400).json({message: `User  Not found`});
    } else {
      res.status(200).json({user: foundUser});
    }
  } catch (error) {
    res.status(500).json({message: error.message});
  }
};

const deleteUser = async (req, res) => {
  const ID = getUserId(req);
  try {
    const user = await User.findById(ID).exec();

    if (!user) {
      res.status(400).send({message: 'User not found'});
    }
    await User.findByIdAndDelete(ID).exec();
    res.status(200).send({message: 'Success'});
  } catch (error) {
    res.status(500).send({message: error.message});
  }
};

const updateUser = async (req, res) => {
  const ID = getUserId(req);
  try {
    const user = await User.findById(ID).exec();
    if (!user) {
      res.status(400).json({message: 'User not found'});
    }

    const comparePwd = await bcrypt.compare(
        req.body.oldPassword,
        user.password,
    );

    if (comparePwd) {
      const hashedPwd = await bcrypt.hash(req.body.newPassword, 10);
      await User.findByIdAndUpdate(ID, {password: hashedPwd}).exec();
    }

    res.status(200).json({message: 'Success'}).end();
  } catch (error) {
    res.status(500).json({message: error.message}).end();
  }
};

module.exports = {getUserById, deleteUser, updateUser};
