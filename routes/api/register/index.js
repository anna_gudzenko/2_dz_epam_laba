const {Router} = require('express');
const router = new Router();
const {handleNewUser} = require('../../../controllers/registerController');

router.post('/', handleNewUser);

module.exports= router;
