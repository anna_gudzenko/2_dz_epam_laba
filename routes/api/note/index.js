const {Router} = require('express');
const router = new Router();
const authMiddleware = require('../../../middleware/authMiddleware');

const {
  postNote,
  getNotes,
  getNoteById,
  updateUsersNote,
  updateCheckedInNote,
  deleteNote,
} = require('../../../controllers/notesControllers');

router
    .post('/', authMiddleware, postNote)
    .get('/', authMiddleware, getNotes)
    .get('/:id', authMiddleware, getNoteById)
    .put('/:id', authMiddleware, updateUsersNote)
    .patch('/:id', authMiddleware, updateCheckedInNote)
    .delete('/:id', authMiddleware, deleteNote);

module.exports = router;
