require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const connectionDB = require('./config/dbConn');
const {logger} = require('./middleware/logevents');

// ConfigService.init();
const app = express();
app.use(logger);
app.use(express.json());

app.use('/api/auth/register', require('./routes/api/register'));
app.use('/api/auth/login', require('./routes/api/login'));
app.use('/api/users/me', require('./routes/api/users'));
app.use('/api/notes', require('./routes/api/note'));

connectionDB();

const port = process.env.APP_PORT || 8080;
mongoose.connection.once('open', () => {
  app.listen(port, () => {
    console.log(`App start on port ${port}`);
  });
});
