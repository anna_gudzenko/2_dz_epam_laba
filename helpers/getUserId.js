const jwt = require('jsonwebtoken');

module.exports = function(req, res) {
  const token = req.headers.authorization.split(' ')[1];
  if (!token) {
    return res.status(403).json({message: 'User is not authorized'});
  }
  const decodedData = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
  return decodedData.id;
};
